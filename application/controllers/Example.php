<?php

class Example extends CI_Controller
{

	public function addUser(){
		$User = new Entities\Example();

		$User->setUsername("asd@asd.com");
		$User->setPassword("ElPassword123");
		$User->setName("El");
		$User->setLastName("Ejemplo");
		$User->setPermissions("permisos");

		try
		{
			$this->doctrine->em->persist($User);
			$this->doctrine->em->flush();
		}
		catch(Exception $e)
		{
			echo "Unexpected error";
		}
	}

	public function updateUser($UserID = null){
		try{
			if(!is_null($UserID)){
				$User = $this->doctrine->em->getRepository('Entities\Example')->findOneBy(array("id" => $UserID));

				if(empty($User)){
					echo "No existe el usuario"; // No hacer echos en los controladores siempre pasar la informacion a la vista, esto es un ejemplo nada mas.
					exit();
				}

				$User->setName("Ojito");

				$this->doctrine->em->merge($User);
				$this->doctrine->em->flush();
			}
			echo "Falto un parametro";
		}
		catch(Exception $e)
		{
			echo "Unexpected error";
		}
	}

	public function removeUser($UserID = null){
		try{
			if(!is_null($UserID)){

				$User = $this->doctrine->em->getRepository('Entities\Example')->findOneBy(array("id" => $UserID));

				if(empty($User)){
					echo "No existe el usuario"; // No hacer echos en los controladores siempre pasar la informacion a la vista, esto es un ejemplo nada mas.
					exit();
				}


				$this->doctrine->em->remove($User);
				$this->doctrine->em->flush();
			}
		}
		catch(Exception $e){

		}
	}

}